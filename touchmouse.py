#!/usr/bin/env python3
#
# Maps a mouse device to an emulated touchscreen. Starting position is the
# center of the device (--width, --height), moving the mouse moves the
# position of the touchpoint. Touch down/up with the left mouse button,
# right mouse button resets position to the center of the device.

import argparse
from libevdev import InputEvent
import libevdev
import time


class TouchDevice(libevdev.Device):
    def __init__(self, width, height):
        super().__init__()
        self.width = width
        self.height = height
        self.is_down = False
        self.tracking_id = 0

        self.name = 'Emulated Touchscreen'
        abs_x = libevdev.InputAbsInfo(minimum=0, maximum=width)
        abs_y = libevdev.InputAbsInfo(minimum=0, maximum=height)

        self.enable(libevdev.EV_KEY.BTN_LEFT)
        self.enable(libevdev.EV_ABS.ABS_X, abs_x)
        self.enable(libevdev.EV_ABS.ABS_Y, abs_y)
        self.enable(libevdev.EV_ABS.ABS_MT_POSITION_X, abs_x)
        self.enable(libevdev.EV_ABS.ABS_MT_POSITION_Y, abs_y)
        self.enable(libevdev.EV_ABS.ABS_MT_SLOT,
                    libevdev.InputAbsInfo(minimum=0, maximum=9))
        self.enable(libevdev.EV_ABS.ABS_MT_TRACKING_ID,
                    libevdev.InputAbsInfo(minimum=0, maximum=65535))
        self.enable(libevdev.INPUT_PROP_DIRECT)
        self.uinput = self.create_uinput_device()
        time.sleep(0.5)  # for userspace to open the device
        self.reset_position()

    def down(self):
        assert not self.is_down
        self.is_down = True
        self.tracking_id += 1
        touch_down = [
            InputEvent(libevdev.EV_ABS.ABS_MT_TRACKING_ID, self.tracking_id),
            InputEvent(libevdev.EV_ABS.ABS_X, self._x),
            InputEvent(libevdev.EV_ABS.ABS_Y, self._y),
            InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, self._x),
            InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, self._y),
            InputEvent(libevdev.EV_KEY.BTN_TOUCH, 1),
            InputEvent(libevdev.EV_SYN.SYN_REPORT, 0),
        ]
        self.uinput.send_events(touch_down)

    def up(self):
        assert self.is_down
        self.is_down = False

        touch_up = [
            InputEvent(libevdev.EV_ABS.ABS_MT_TRACKING_ID, -1),
            InputEvent(libevdev.EV_KEY.BTN_TOUCH, 0),
            InputEvent(libevdev.EV_SYN.SYN_REPORT, 0),
        ]
        self.uinput.send_events(touch_up)

    def reset_position(self):
        self.x = self.width // 2
        self.y = self.height // 2
        self.move()

    def move(self):
        if not self.is_dirty:
            return
        touch_move = [
            InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, self._x),
            InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, self._y),
            InputEvent(libevdev.EV_SYN.SYN_REPORT, 0),
        ]
        self.uinput.send_events(touch_move)

    def rel_move(self, x=0, y=0):
        if x != 0:
            self.x = min(max(self.x + x, 0), self.width)
        if y != 0:
            self.y = min(max(self.y + y, 0), self.height)
        self.move()

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x
        self.is_dirty = True

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y
        self.is_dirty = True

    def __str__(self):
        return ('Touch device {}: {}'.format(self.uinput.name, self.uinput.devnode))


def main():
    parser = argparse.ArgumentParser('uinput-emulated touchscreen mapping to a mouse')
    parser.add_argument('source_device', type=str)
    parser.add_argument('--width', type=int, default=1920)
    parser.add_argument('--height', type=int, default=960)
    ns = parser.parse_args()

    touch = TouchDevice(ns.width, ns.height)
    print(touch)
    mouse = libevdev.Device(open(ns.source_device, 'rb'))
    mouse.grab()
    print('Taking over {}: {}'.format(mouse.name, ns.source_device))

    x, y = 0, 0

    while True:
        for e in mouse.events():
            if e.matches(libevdev.EV_KEY.BTN_LEFT, value=1):
                touch.down()
            elif e.matches(libevdev.EV_KEY.BTN_LEFT, value=0):
                touch.up()
            elif e.matches(libevdev.EV_KEY.BTN_RIGHT, value=0):
                touch.reset_position()
            elif e.matches(libevdev.EV_REL.REL_X):
                x = e.value
            elif e.matches(libevdev.EV_REL.REL_Y):
                y = e.value
            elif e.matches(libevdev.EV_SYN.SYN_REPORT):
                touch.rel_move(x, y)
                x, y = 0, 0
                print('\rPosition: {}/{} {:4s}'.format(touch.x, touch.y, 'down' if touch.is_down else ''), end='')


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
