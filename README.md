# Touchmouse

Touchmouse is a touch screen emulator which can be used with a normal USB
mouse.

Assuming your mouse is device `/dev/input/event3`, run touchmouse like this:

```
   sudo python3 touchmouse.py /dev/input/event3
```

Moving the mouse will moves the virtual position of the touch screen,
clipping it to within the device's axis ranges so you will stay on whatever
screen the touch device is assigned to by your desktop environment.

The left mouse button triggers a touch down/up at the current position,
dragging the mouse will move the touch around. The right mouse button resets
the touch position to the center of the screen.

Touchmouse requires root permissions to be able to create a uinput device.

If the movement is too slow (1 mouse unit == 1 touchscreen unit), reduce the
size of the touchscreen with the `--width` and `--height` arguments. The
width/height of the touchscreen will be mapped to the output, so the smaller
the touchscreen resolution, the more output pixels you will cover by one
unit of movement.

## Installation

touchmouse requires `python-libevdev`, you can get that from `pip` with

```
pip3 install libevdev
```

touchmouse itself is not intended to be installed, it's a debugging utility.
